'use strict';

/* eslint-disable no-alert, no-console */

const request = require('request-promise');
/**
 * Contains settings for the app.
 * @type {object}
 */
const options = {
  retrieveUrl: '',
  newApiUrl: 'http://localhost:8080/articles',
  // search for articles with pubdate this many seconds before or after current time
  articleIntervall: 60 * 60,
  // How many articles to get per API request
  articleLimit: 25,
  // How often to check for new articles (seconds)
  pollIntervall: 5,
  // Log converted data to console?
  showConverted: false,
};

/**
 * Flag that indicates if a process is currently running
 * @type {boolean}
 */
let processRunning = false;

/**
 * Console timer
 *
 * @returns number timer setInterval Id
 */
function twirlTimer() {
  const P = ['\\', '|', '/', '-'];
  let x = 0;
  return setInterval(() => {
    process.stdout.write('\r');
    process.stdout.write(P[x += 1]);
    if (x === 3) {
      x = 0;
    }
  }, 1000);
}

/**
 * Array containg the articles processed in the last cycle.
 * Used so that we don't convert and send the same articles over and over again to the new API.
 * @type {array}
 */
let previouslyProcessed = [];

/**
 * Stores info about when next cycle will run
 */
let nextPollCycleStarts;

/**
 * Stores the timer object
 */
let timer = false;

function setNextPollTime() {
  const pollTime = new Date();
  pollTime.setSeconds(pollTime.getSeconds() + options.pollIntervall);
  nextPollCycleStarts = pollTime.toLocaleString();
}

/**
 * Create and return a object containing start and end iso strings.
 * @returns {Object}
 */
function getIsoStringTimes() {
  // Start time is options.articleIntervall ago
  let startinclusive = new Date();
  startinclusive.setSeconds(startinclusive.getSeconds() - options.articleIntervall);
  startinclusive = startinclusive.toISOString();

  // End time is options.articleIntervall from now
  let endexclusive = new Date();
  endexclusive.setSeconds(endexclusive.getSeconds() + options.articleIntervall);
  endexclusive = endexclusive.toISOString();

  return {
    start: startinclusive,
    end: endexclusive,
  };
}

/**
 * Fetches articles from paginated api.
 *
 * @param {object}   searchInterval - Object with 'start' and 'end' isoString properties.
 * @param {number}   startIndex     - "Page" to fetch from.
 * @param {array}    articles       - Previously fethed articles.
 * @returns {array}
 */
function getArticles(searchInterval, startNbr, articlesArr) {
  let startIndex = startNbr;
  if (!startIndex) {
    startIndex = 0;
  }

  let articles = articlesArr;
  if (!articles) {
    articles = [];
  }

  // Set up the query parameters for the get request
  const queryParams = {
    'Pubdate.startinclusive': searchInterval.start,
    'Pubdate.endexclusive': searchInterval.end,
    'sort.name': 'Publiceringsdag',
    contenttype: 'Article',
    start: startIndex,
    limit: options.articleLimit,
    q: '*:*',
  };

  // Requests articles from old API
  return request({
    method: 'GET',
    uri: options.retrieveUrl,
    qs: queryParams,
    json: true,
  }).then((response) => {
    articles = articles.concat(response.hits.hits);
    console.log('articles fetched:', articles.length);
    console.log('totalHits', response.hits.totalHits);
    // Check if request shoud be run again.
    if (response.hits.totalHits > articles.length) {
      console.log('Fetch more articles...');
      return getArticles(searchInterval, startIndex + options.articleLimit, articles);
    }

    return articles;
  })
  .catch((err) => {
    console.error(err.name);
    console.error(err.message);
    // Return an empty array on failure.
    return [];
  });
}

/**
 * Contains functions that handles the checking of new or changed articles.
 * @type {object}
 */
const checker = {
  /**
   * Checks for articles that are new or have changed
   *
   * @param {Array} articles
   * @returns {Object} Object containing arrays of fetched an new articles.
   */
  checkForNewData(articles) {
    // Store articles that has already been processed here
    const oldArticles = [];
    // Store new articles here
    const newArticles = [];
    // Count of articles processed last cycle
    const noProcessed = previouslyProcessed.length;

    // Loop articles and check if they are new
    articles.forEach((article) => {
      let newArticle = true;
      for (let i = 0; i < noProcessed; i += 1) {
        // Check if matching id can be found in array of articles from prevous cycle
        if (previouslyProcessed[i].id === article.id) {
          // ID found but check if article has new versions
          const newVersions = checker.checkArticleVersions(article, previouslyProcessed[i]);
          if (newVersions.length > 0) {
            console.log('new version!');
            break;
          }
          // No new version found.
          newArticle = false;
          // Push to array of old artices
          oldArticles.push(article);
          break;
        }
      }

      if (newArticle) {
        // Create a new object and push to array of new articles
        newArticles.push(article);
      }
    });

    // Return object containing old and new article arrays
    return {
      old: oldArticles,
      new: newArticles,
    };
  },

  /**
   * Checks if an article has new version(s) by comparung the version arrays
   *
   * @param {object} article
   * @param {object} oldArticle
   * @returns {array} Array of new article version id:s
   */
  checkArticleVersions(article, oldArticle) {
    const newVersions = [];
    const nbrOfOldVersions = oldArticle.versions.length;
    article.versions.forEach((newVersion) => {
      let thisIsNew = true;
      for (let i = 0; i < nbrOfOldVersions; i += 1) {
        if (oldArticle.versions[i].id === newVersion.id) {
          thisIsNew = false;
          break;
        }
      }
      if (thisIsNew) {
        newVersions.push(newVersion.id);
      }
    });
    // Check for new article versions here
    return newVersions;
  },
};

/**
 * Contains functions that handles the conversion of articles.
 * @type {object}
 */
const converter = {
  /**
   * Create a new object with converted data
   */
  convertArticle: (article) => {
    const convertedArticle = {
      id: article.id,
      versions: [],
    };

    article.versions.forEach((version) => {
      const convertedVersion = converter.convertArticleProperties(version.properties);
      convertedVersion.id = version.id;
      convertedArticle.versions.push(convertedVersion);
    }, this);
    return convertedArticle;
  },

  /**
   * Convert properties
   */
  convertArticleProperties: (properties) => {
    const newProperties = {
      uuid: converter.convertPropertyValue(properties.uuid),
      date_created: converter.convertPropertyValue(properties.created),
      date_published: converter.convertPropertyValue(properties.Pubdate),
      date_updated: converter.convertPropertyValue(properties.Updated),
      headline: converter.convertPropertyValue(properties.Headline),
      quoted_headline: converter.convertPropertyValue(properties.QuoteHeadline),
      pre_leadin: converter.convertPropertyValue(properties.ArticlePreleadin),
      leadin: converter.convertPropertyValue(properties.ArticleLeadin),
      body: converter.convertPropertyValue(properties.Text),
      image_captions: properties.ArticleCaption,
      image_uuids: properties.ImageUuid,
      author: {
        name: converter.convertPropertyValue(properties.Author),
        byline: converter.convertPropertyValue(properties.AuthorByline),
        phone: converter.convertPropertyValue(properties.AuthorPhone),
        email: converter.convertPropertyValue(properties.AuthorEmail),
      },
      fact: {
        headline: converter.convertPropertyValue(properties.FactHeadline),
        leadin: converter.convertPropertyValue(properties.FactLeadin),
        body: converter.convertPropertyValue(properties.FactBody),
      },
      teaser: {
        headline: converter.convertPropertyValue(properties.TeaserHeadline),
        image_uuid: converter.convertPropertyValue(properties.TeaserImageUuid),
        vignette: converter.convertPropertyValue(properties.TeaserVignette),
        drophead: converter.convertPropertyValue(properties.TeaserDrophead),
        body: converter.convertPropertyValue(properties.TeaserBody),
      },
      location: converter.convertPropertyValue(properties.Location),
      categories: properties.Categories,
      department: converter.convertPropertyValue(properties.department),
      sections: properties.Sections,
      section_id: converter.convertPropertyValue(properties.Sections),
      section_short: converter.convertPropertyValue(properties.SectionShort),
      tags: properties.Tags,
      priority: converter.convertPropertyValue(properties.UserfieldpriorityArticle),
      type: converter.convertPropertyValue(properties.ArticleType),
      sub_product: properties.SubProductShort,
      related_articles_uuid: properties.RelatedArticlesUuid,
      related_links: properties.RelatedLinks,
    };

    return newProperties;
  },

  /**
   * Convert some properties that probably don't need to be arrays
   */
  convertPropertyValue: (property) => {
    if (typeof property === 'undefined' || property == null) {
      return null;
    }
    if (Array.isArray(property)) {
      if (property.length > 0) {
        return property[0];
      }
      return null;
    }
    return property;
  },
};

/**
 * Sends new articles to the api one by one.
 *
 * @param {object}  article - Article object to send
 * @returns {boolean}
 */
function sendToNewApi(article) {
  // Convert article to new format
  const convertedArticle = converter.convertArticle(article);

  // Fake request, pretend it takes two seconds for the new API to respond
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const rand = Math.floor(Math.random() * 3) + 1; // Random number between 1 - 3
      return rand === 2 ? reject({ name: 'request error', message: 'fake request error ' }) : resolve();
    }, 2000);
  }).then(() => {
    console.log('OK:');
    console.log(convertedArticle.versions[0].date_published);
    console.log(convertedArticle.versions[0].headline);
    if (options.showConverted) {
      console.log('Converted succesfully sent to new API:');
      console.log(JSON.stringify(convertedArticle, null, 2));
    }
    return article;
  })
  .catch((err) => {
    console.error(err.name);
    console.error(err.message);
    console.error(convertedArticle.versions[0].date_published);
    console.error(convertedArticle.versions[0].headline);
    return false;
  });
}

/**
 * Send articles to the new api.
 * Waits for all request to be done before returning object containg
 * array succesfully sent articles and number of errors.
 *
 * @param {array} articles
 * @returns
 */
function sendArticles(articles) {
  const nbrOfArticles = articles.length;
  const promises = [];
  for (let i = 0; i < nbrOfArticles; i += 1) {
    promises.push(sendToNewApi(articles[i]));
  }

  return Promise.all(promises).then((results) => {
    const ok = results.filter(result => result !== false);
    return { ok, errors: nbrOfArticles - ok.length };
  }).catch(err => err);
}

function processDone(stats) {
  console.log('done');
  console.log(JSON.stringify(stats, null, 2));
  // Set flag to false and allow next cycle to start
  processRunning = false;
  console.log('Next poll starts at:', nextPollCycleStarts);
  console.log('poll interval:', options.pollIntervall);
  timer = twirlTimer();
}

/**
 * The main function that fetches, processes and sends the data.
 *
 */
function main() {
  // Stores the time when next cycle will be run.
  setNextPollTime();
  // Check that a cycle is not currently running
  if (processRunning) {
    console.log('Waiting for previous process to finnish...');
    return;
  }

  // Hides the timer animation
  if (timer !== false) {
    clearInterval(timer);
    timer = false;
  }

  // Set process flag to true
  processRunning = true;

  const searchInterval = getIsoStringTimes();
  const stats = {};
  console.log('Search for articles with pubdates between ', searchInterval.start);
  console.log('and ', searchInterval.end);
  // Fetch articles from old api
  getArticles(searchInterval)
  .then((result) => {
    stats.fetched = result.length;
    if (result.length < 1) {
      return processDone(stats);
    }
    // Check if there are new articles
    const checkedArticles = checker.checkForNewData(result);
    const oldArticles = checkedArticles.old;
    const newArticles = checkedArticles.new;
    stats.new = newArticles.length;
    if (newArticles.length < 1) {
      return processDone(stats);
    }
    // Send new artcicles to the new API
    sendArticles(newArticles).then((sentArticles) => {
      stats.sent_succesfully = sentArticles.ok.length;
      stats.sent_unsuccesfully = sentArticles.errors;

      if (sentArticles.ok.length > 0) {
        // Save data of this cycle
        previouslyProcessed = oldArticles.concat(sentArticles.ok);
      }

      return processDone(stats);
    })
    .catch((err) => {
      console.error('Error when sending articles to API');
      console.error(err);
      return processDone(stats);
    });
    // This should never happen
    return false;
  });
}


/**
 * Initialize app
 *
 * @returns void
 */
function init() {
  if (typeof process.argv[2] === 'undefined') {
    console.error('API url missing!');
    return;
  }
  options.retrieveUrl = process.argv[2];
  main();

  // Start polling
  setInterval(main, options.pollIntervall * 1000);
}

init();
