# API UTILILITY

Polls old API and sends (converted) results to new API.

## Requirements
- Node.js

## Install
`npm install`

## Run
`node app.js 'http://url.to/old/api'`

Where "http://url.to/old/api" is an actual working api uri
